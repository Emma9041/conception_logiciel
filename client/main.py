import json, requests, random, os
from dotenv import load_dotenv

load_dotenv()


with open("client/rudy.json", "r", encoding="utf-8") as read_file:
    data = json.load(read_file)


def generate_playlist():
    artists= list(map(lambda x:x["artiste"],data))
    weights= list(map(lambda x:x["note"],data))
    playlist = []
    base_format = "N.{} : {} by {} \n (Suggested youtube URL:{}) \n Lyrics : \n {} \n "
    while len(playlist)<20: 
        artiste = random.choices(artists, weights=weights)[0]
        song = requests.get(os.environ.get("server_URL")+"/random/"+artiste).json()

        if song["lyrics"] != None and song not in playlist: 
            #filtering songs with no lyrics (necessary for karaoke) and those already in the playlist
            playlist.append(song)
            print(base_format.format(len(playlist),song["title"],song["artist"],song["suggested_youtube_url"],song["lyrics"]))

if __name__ == "__main__":
    generate_playlist()

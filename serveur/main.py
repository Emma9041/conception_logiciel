from dotenv import load_dotenv
import os, requests, random
from fastapi import FastAPI


load_dotenv()

app = FastAPI()


@app.get("/")
def healthcheck():
    r_lyricsovh_api = requests.get(os.environ.get("lyrics_URL")+"/beatles/across the universe")
    r_audiodb_api = requests.get(os.environ.get("artist_URL")+"/rihanna")
    return r_lyricsovh_api.status_code == 200 and r_audiodb_api.status_code == 200


def getIdArtist(requestAsJson):
    try:
        id_artist = requestAsJson["artists"][0]["idArtist"]
        return {"id_artist": id_artist}
    except:
        print("Artist request failed")

    
def getRandomAlbum(requestAsJson):
    try:
        random_int = random.randint(0, len(requestAsJson["album"])-1)
        id_album = requestAsJson["album"][random_int]["idAlbum"]
        return {"id_album": id_album}
    except:
        print("Album request failed")


def getRandomTrack(requestAsJson):
    try:
        random_int = random.randint(0, len(requestAsJson["track"])-1)
        track = requestAsJson["track"][random_int]
        title_track = track["strTrack"]
        url = track["strMusicVid"]
        return {"title": title_track, "url": url}
    except:
        print("Track request failed")


def getTrackLyrics(requestAsJson):
    try:
        if "lyrics" in requestAsJson:
            lyrics =  requestAsJson["lyrics"] 
        else:
            lyrics = None
        return {"lyrics": lyrics}
    except:
        print("Lyrics request failed")


@app.get("/random/{artist_name}")
def read_artiste(artist_name):
    id_artist = getIdArtist(requests.get(os.environ.get("artist_URL")+artist_name).json())["id_artist"]
    id_album = getRandomAlbum(requests.get(os.environ.get("albums_URL")+id_artist).json())["id_album"]
    track = getRandomTrack(requests.get(os.environ.get("tracks_URL")+id_album).json())
    title = track["title"]
    url = track["url"]
    #on supprime les "/" dans les titres de chansons ci-dessous car ce caractère génère une erreur si il est présent dans l'url)
    print(artist_name)
    print(title.replace("/",""))
    lyrics = getTrackLyrics(requests.get(os.environ.get("lyrics_URL")+"/{}/{}".format(artist_name,title.replace("/",""))).json())["lyrics"]
    return {
    "artist": artist_name,
    "title": title,
    "suggested_youtube_url": url,
    "lyrics": lyrics
}

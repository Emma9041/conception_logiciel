# Conception de logiciel: Chansons aléatoires et playlist

## Fonctionnalités:

-afficher une chanson aléatoire d'un artiste (ainsi que ses paroles et son url youtube si disponibles)

-générer une playlist de chansons à partir d'une liste d'artistes

## Guide d'utilisation

### Pour cloner
```
git clone https://gitlab.com/Emma9041/conception_logiciel.git
```

### Prérequis

```
cd conception_logiciel

pip install -r client/requirements.txt

pip install -r serveur/requirements.txt
```

### Pour lancer l'application
```
cd serveur

uvicorn main:app --reload
```

### Puis, se rendre sur l'url:

- http://localhost:8000/random/{nomdelartiste} pour obtenir une chanson aléatoire d'un artiste souhaité

- http://localhost:8000 pour vérifier l'état des api auxquelles le serveur fait appel (la page doit afficher true)

### Ou, pour obtenir une playlist pour Rudy basée sur le fichier rudy.json, ouvrir un second terminal à la racine du projet puis:

```
python client/main.py
```

### Pour lancer les tests

```
pytest tests/tests_serveur.py
```

### Schéma d'architecture

```mermaid

graph TD;

  Client -- Requête --> Serveur;

  Serveur -- Réponse --> Client;

  Serveur -- Requête --> AudioDB;

  Serveur -- Requête --> Lyricsovh;

  AudioDB -- Réponse --> Serveur ;

  Lyricsovh -- Réponse --> Serveur ;

```


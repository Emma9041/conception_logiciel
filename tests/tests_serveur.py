import random
from conception_logiciel.serveur.main import getIdArtist, getRandomAlbum
import json

def load_params_from_json(json_path):
    with open(json_path) as f:
        return json.load(f)

def test_id_artist():
    data=load_params_from_json('tests/datatest_idartist.json')
    assert getIdArtist(data) == {"id_artist": "111305"}

def test_random_album():
    random.seed(1)
    data=load_params_from_json('tests/datatest_randomalbum.json')
    assert getRandomAlbum(data) == {'id_album': '2109888'}
